<?php

// configure
$from = 'Contact form <gmecv@gme.com.ar>';
$sendTo = 'gabrieleguia@gmail.com';
$subject = 'Nuevo mensaje del formulario de contacto de GME CV';
$fields = array('name' => 'Name', 'email' => 'Email', 'message' => 'Message'); // array variable name => Text to appear in the email
$okMessage = 'Contact form successfully submitted. Thank you, I will get back to you soon!';
$errorMessage = 'There was an error while submitting the form. Please try again later';

// let's do the sending

if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])):
    //your site secret key
    $secret = '6LfYMOcUAAAAAITrn03h1TXrraoIH-OA73KOwbH7';
    //get verify response data

    $c = curl_init('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    $verifyResponse = curl_exec($c);

    $responseData = json_decode($verifyResponse);
    if($responseData->success):

        try
        {
            $emailText = nl2br("You have new message from Contact Form\n");

            $name = "";

            if (isset($_POST['name']))
                $name = @trim(stripslashes($_POST['name']));

            if (isset($_POST['email']))
                $from = @trim(stripslashes($_POST['email']));

            if (isset($_POST['subject']))
                $subject = @trim(stripslashes($_POST['subject']));

            if (isset($_POST['message']))
                $emailText .= @trim(stripslashes($_POST['message']));

            $headers = array('Content-Type: text/plain;',
                "MIME-Version: 1.0",
                'From: Contact form <gmecv@gme.com.ar>',
                'Reply-To: ' . $name . ' <' . $from . '>',
                'Subject: ' . $subject,
                "X-Mailer: PHP/".phpversion()
            );

            mail($sendTo, $subject, $emailText, implode("\n", $headers));

            $responseArray = array('type' => 'success', 'message' => $okMessage);
        }
        catch (\Exception $e)
        {
            $responseArray = array('type' => 'danger', 'message' => $errorMessage);
        }

        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $encoded = json_encode($responseArray);

            header('Content-Type: application/json');

            echo $encoded;
        }
        else {
            echo $responseArray['message'];
        }

    else:
        $errorMessage = 'Robot verification failed, please try again.';
        $responseArray = array('type' => 'danger', 'message' => $errorMessage);
        $encoded = json_encode($responseArray);

            header('Content-Type: application/json');

            echo $encoded;
    endif;
else:
    $errorMessage = 'Please click on the reCAPTCHA box.';
    $responseArray = array('type' => 'danger', 'message' => $errorMessage);
    $encoded = json_encode($responseArray);

            header('Content-Type: application/json');

            echo $encoded;
endif;

